import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsertodosComponent } from './usertodos.component';

describe('UsertodosComponent', () => {
  let component: UsertodosComponent;
  let fixture: ComponentFixture<UsertodosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsertodosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsertodosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
